package com.sunbeaminfo.sh.onlinefood.controller;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunbeaminfo.sh.onlinefood.utils.HbUtil;

public class ShopController extends HttpServlet {
	private Properties routeProps = new Properties();
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		HbUtil.getSession(); // just to start hibernate while deploying appln
		String routes = "/route.properties";
		String routeFile = config.getInitParameter("routes");
		if(routeFile != null)
			routes = routeFile;
		System.out.println("Loading routes from : " + routes);
		try (InputStream in = ShopController.class.getResourceAsStream(routes)) {
			routeProps.load(in);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		processRequest(req, resp);
	}

	protected void processRequest(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		String page = req.getParameter("page");
		String pageUrl = routeProps.getProperty(page);

		System.out.println("Requested : " + page + " & Forwarding to : " + pageUrl);
		
		if(pageUrl != null && !pageUrl.isEmpty()) {
			ServletContext ctx = this.getServletContext();
			RequestDispatcher rd = ctx.getRequestDispatcher(pageUrl);
			rd.forward(req, resp);
		} else {
			ServletContext ctx = this.getServletContext();
			RequestDispatcher rd = ctx.getRequestDispatcher("/notfound.jsp");
			rd.forward(req, resp);
		}
	}
}
