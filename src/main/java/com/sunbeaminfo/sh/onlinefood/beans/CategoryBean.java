package com.sunbeaminfo.sh.onlinefood.beans;

import java.util.ArrayList;
import java.util.List;

import com.sunbeaminfo.sh.onlinefood.daos.ItemDao;

public class CategoryBean {
	private List<String> categories;
	public CategoryBean() {
		this.categories = new ArrayList<String>();
	}
	public List<String> getCategories() {
		return categories;
	}
	public void setCategories(List<String> categories) {
		this.categories = categories;
	}
	public void fetchCategories() {
		try(ItemDao dao = new ItemDao()) {
			dao.open();
			this.categories = dao.getCategories();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
