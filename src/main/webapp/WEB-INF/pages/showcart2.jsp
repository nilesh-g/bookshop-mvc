<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="shop" uri="/WEB-INF/shop.tld" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Show Cart2</title>
</head>
<body>
	Using Custom Tags
	<c:set var="total" value="0.0"/>
	<table border="1">
		<thead style="background-color: gray;">
			<tr>
				<td>Id</td>
				<td>Item</td>
				<td>Category</td>
				<td>Price</td>
			</tr>
		</thead>
	<c:forEach var="id" items="${scb.cart}">
		<shop:food id="${id}"/>
	</c:forEach>
	</table>	
	<br/><br/>
	Total Bill: Rs. ${total}/-
	<br/><br/>
	<a href="ctl?page=logout">Sign Out</a>
</body>
</html>



